/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#include "fluilbcdec.h"
#include "fluilbcenc.h"

GST_DEBUG_CATEGORY (gst_fluilbcdec_debug);
GST_DEBUG_CATEGORY (gst_fluilbcenc_debug);

static gboolean
plugin_init (GstPlugin * plugin)
{

  GST_DEBUG_CATEGORY_INIT (gst_fluilbcdec_debug, "fluilbcdec", 0,
      "Fluendo iLBC decoder");

  GST_DEBUG_CATEGORY_INIT (gst_fluilbcenc_debug, "fluilbcenc", 0,
      "Fluendo iLBC encoder");

  if (!gst_element_register (plugin, "fluilbcdec", GST_RANK_PRIMARY + 1,
          gst_fluilbcdec_get_type ()))
    return FALSE;

  if (!gst_element_register (plugin, "fluilbcenc", GST_RANK_PRIMARY + 1,
          gst_fluilbcenc_get_type ()))
    return FALSE;

  return TRUE;
}

FLUENDO_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "fluilbc",
    fluilbc,
    "Fluendo iLBC decoder/encoder",
    plugin_init,
    VERSION,
    GST_LICENSE_UNKNOWN, "Fluendo iLBC decoder/encoder", "http://www.fluendo.com");
