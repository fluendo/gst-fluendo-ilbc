/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

GST_BOILERPLATE (GstFluILBCDec, gst_fluilbcdec, GstElement, GST_TYPE_ELEMENT);

static GstElementDetails fluilbcdec_details = {
  PLUGIN_DESC, PLUGIN_CAT, PLUGIN_LONGDESC, PLUGIN_CR
};

static GstStaticPadTemplate fluilbcdec_src_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int, "
        "rate = (int) 8000, "
        "channels = (int) 1, "
        "width = (int) 16, "
        "depth = (int) 16, "
        "signed = (boolean) true, "
        "endianness = (int) BYTE_ORDER")
    );

static GstStaticPadTemplate fluilbcdec_sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-iLBC, mode = (int) { 20, 30 }")
    );

#define GST_FLOW_NEED_MORE_DATA -101

static gboolean
gst_fluilbcdec_setcaps (GstPad * pad, GstCaps * caps)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (gst_pad_get_parent (pad));
  GstStructure *structure;
  GstCaps *out_caps;
  gboolean ret = FALSE;

  GST_DEBUG_OBJECT (dec, "setcaps called with %" GST_PTR_FORMAT, caps);

  structure = gst_caps_get_structure (caps, 0);

  if (!gst_structure_get_int (structure, "mode", &dec->mode)) {
    GST_WARNING_OBJECT (dec, "missing mode value in caps");
    dec->mode = 20;
  }

  gst_fluilbcdec_setup (dec);

  out_caps = gst_caps_new_simple ("audio/x-raw-int",
      "channels", G_TYPE_INT, 1,
      "width", G_TYPE_INT, 16,
      "depth", G_TYPE_INT, 16,
      "rate", G_TYPE_INT, RATE,
      "signed", G_TYPE_BOOLEAN, TRUE,
      "endianness", G_TYPE_INT, G_BYTE_ORDER, NULL);

  gst_pad_set_caps (dec->srcpad, out_caps);
  gst_caps_unref (out_caps);
  dec->duration = gst_util_uint64_scale (dec->framesize >> 1, GST_SECOND, RATE);

  ret = TRUE;

//beach:
  gst_object_unref (dec);
  return ret;
}

static inline GstBuffer *
gst_fluilbcdec_clip (GstFluILBCDec * dec, GstBuffer * buffer)
{
  gint64 clip_start = 0, clip_stop = 0, start = 0, stop = 0;
  gboolean in_seg = FALSE;
  gint64 buf_size = 0;

  buf_size = GST_BUFFER_SIZE (buffer);
  start = GST_BUFFER_TIMESTAMP (buffer);
  stop = start + GST_BUFFER_DURATION (buffer);

  in_seg = gst_segment_clip (dec->segment, GST_FORMAT_TIME,
      start, stop, &clip_start, &clip_stop);

  if (in_seg) {
    guint start_offset = 0, stop_offset = buf_size;

    /* We have to remove some heading samples */
    if (clip_start > start && clip_start <= stop) {
      start_offset = gst_util_uint64_scale_int (clip_start - start,
          RATE, GST_SECOND) * 2;
    }
    /* We have to remove some trailing samples */
    if (clip_stop < stop && clip_stop >= start) {
      stop_offset -= gst_util_uint64_scale_int (stop - clip_stop,
          RATE, GST_SECOND) * 2;
    }
    /* Truncating */
    if ((start_offset != 0) || (stop_offset != buf_size)) {
      GstBuffer *subbuf = gst_buffer_create_sub (buffer, start_offset,
          stop_offset - start_offset);
      if (subbuf) {
        gst_buffer_unref (buffer);
        buffer = subbuf;
        /* gst_buffer_create_sub don't crate it with caps */
        gst_buffer_set_caps (buffer, GST_PAD_CAPS (dec->srcpad));
      }
    }

    GST_BUFFER_TIMESTAMP (buffer) = clip_start;
    GST_BUFFER_DURATION (buffer) = clip_stop - clip_start;

    if (dec->discont) {
      GST_DEBUG_OBJECT (dec, "mark this frame with the discont flag");
      GST_BUFFER_FLAG_SET (buffer, GST_BUFFER_FLAG_DISCONT);
      dec->discont = FALSE;
    }

    GST_DEBUG_OBJECT (dec, "push a buffer with timestamp %" GST_TIME_FORMAT
        ", duration %" GST_TIME_FORMAT ", samples %d",
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buffer)),
        GST_TIME_ARGS (GST_BUFFER_DURATION (buffer)),
        (stop_offset - start_offset) / 2);

    dec->last_timestamp = clip_stop;
  } else {
    GST_DEBUG_OBJECT (dec, "dropping buffer because is out of segment,"
        "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT,
        GST_TIME_ARGS (start), GST_TIME_ARGS (stop));
    gst_buffer_unref (buffer);
    buffer = NULL;
  }

  return buffer;
}

static GstFlowReturn
gst_fluilbcdec_decode (GstFluILBCDec * dec)
{
  GstFlowReturn ret = GST_FLOW_OK;
  const guint8 *in_data;
  gint16 *out_data;
  guint32 in_size = dec->packetsize;
  guint32 out_size = dec->framesize;
  GstBuffer *buffer;
  guint avail = 0;

  avail = gst_adapter_available (dec->adapter);
  /* 1 byte for the TOC */
  if (avail < in_size) {
    GST_LOG_OBJECT (dec, "not enough data for a header");
    ret = GST_FLOW_NEED_MORE_DATA;
    goto beach;
  }
  /* read the packet */
  in_data = gst_adapter_peek (dec->adapter, in_size);

  GST_DEBUG_OBJECT (dec, "Packet number %" G_GUINT64_FORMAT, dec->packet_count);

  /* decode the packet */
  iLBC_decode (dec->block, (unsigned char *) in_data, &dec->codec, 1);

  /* flush the packet in the adapter */
  gst_adapter_flush (dec->adapter, in_size);
  dec->packet_count++;

  /* allocate a buffer for the output samples */
  buffer = gst_buffer_new_and_alloc (out_size);
  if (!GST_IS_BUFFER (buffer)) {
    GST_WARNING_OBJECT (dec, "failed allocating a %d bytes buffer", out_size);
    ret = GST_FLOW_ERROR;
    goto beach;
  }

  gst_buffer_set_caps (buffer, GST_PAD_CAPS (dec->srcpad));
  /* calculate timestamp and duration */
  GST_BUFFER_TIMESTAMP (buffer) = dec->last_timestamp;
  GST_BUFFER_DURATION (buffer) = dec->duration;
  if (dec->last_timestamp != -1) {
    dec->last_timestamp += GST_BUFFER_DURATION (buffer);
  }

  /* Convert and write decoded samples */
  out_data = (gint16 *) GST_BUFFER_DATA (buffer);
  gst_fluilbcdec_write_samples (dec, out_data);

  /* perform clipping in segment and push */
  if ((buffer = gst_fluilbcdec_clip (dec, buffer))) {
    ret = GSTFLU_PAD_PUSH (dec->srcpad, buffer, &dec->stats);
  }
  
beach:
  return ret;
}

static GstFlowReturn
gst_fluilbcdec_chain (GstPad * pad, GstBuffer * buffer)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (gst_pad_get_parent (pad));
  GstFlowReturn ret = GST_FLOW_OK;

  GST_DEBUG_OBJECT (dec, "new buffer in chain");

  /* discontinuity clears adapter, FIXME, maybe we can set some
   * encoder flag to mask the discont. */
  if (GST_BUFFER_FLAG_IS_SET (buffer, GST_BUFFER_FLAG_DISCONT)) {
    gst_adapter_clear (dec->adapter);
    dec->last_timestamp = 0;
    dec->discont = TRUE;
  }

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer)) {
    dec->last_timestamp = GST_BUFFER_TIMESTAMP (buffer);
  }

  gst_adapter_push (dec->adapter, buffer);

  while (ret == GST_FLOW_OK) {
    ret = gst_fluilbcdec_decode (dec);
  }
  if (ret == GST_FLOW_NEED_MORE_DATA) {
    GST_LOG_OBJECT (dec, "need more data");
    ret = GST_FLOW_OK;
  }

  gst_object_unref (dec);

  return ret;
}

static void
gst_fluilbcdec_flush (GstFluILBCDec * dec, gboolean discard)
{
  if (!discard) {
    guint avail = gst_adapter_available (dec->adapter);
    if (avail && dec->packetsize) {
      guint size = dec->packetsize - avail;
      GstBuffer *buffer = gst_buffer_new_and_alloc (size);
      memset (GST_BUFFER_DATA (buffer), 0, size);
      gst_adapter_push (dec->adapter, buffer);
      gst_fluilbcdec_decode (dec);
    }
  }
  dec->last_timestamp = 0;
  dec->discont = FALSE;
  dec->eos = FALSE;
  dec->packet_count = 0;
  gst_adapter_clear (dec->adapter);
}

static gboolean
gst_fluilbcdec_event (GstPad * pad, GstEvent * event)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (gst_pad_get_parent (pad));
  gboolean ret = FALSE;

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
    {
      GST_DEBUG_OBJECT (dec, "we are EOS");
      gst_fluilbcdec_flush (dec, FALSE);
      dec->eos = TRUE;
      dec->packet_count = 0;
      ret = gst_pad_event_default (pad, event);
      break;
    }
    case GST_EVENT_NEWSEGMENT:
    {
      GstFormat format;
      gdouble rate;
      gint64 start, stop, time;
      gboolean update;

      gst_event_parse_new_segment (event, &update, &rate, &format, &start,
          &stop, &time);

      GST_DEBUG_OBJECT (dec, "received new segment from %" GST_TIME_FORMAT
          " to %" GST_TIME_FORMAT, GST_TIME_ARGS (start), GST_TIME_ARGS (stop));

      if (!update) {
        gst_fluilbcdec_flush (dec, FALSE);
        if (format == GST_FORMAT_TIME) {
          dec->last_timestamp = start;
        }
      }
      if (format != GST_FORMAT_TIME) {
        format = GST_FORMAT_TIME;
        start = time = 0;
        stop = -1;
        gst_event_unref (event);
        event = gst_event_new_new_segment (update, rate, format,
            start, stop, time);
      }
      /* now copy over the values */
      gst_segment_set_newsegment (dec->segment, update, rate, format, start,
          stop, time);


      ret = gst_pad_event_default (pad, event);
      break;
    }
    case GST_EVENT_FLUSH_STOP:
      GST_DEBUG_OBJECT (dec, "flushing");

      gst_fluilbcdec_flush (dec, TRUE);

      /* Need to init our segment again after a flush */
      gst_segment_init (dec->segment, GST_FORMAT_TIME);

      ret = gst_pad_event_default (pad, event);
      break;
    default:
      ret = gst_pad_event_default (pad, event);
      break;
  }

  gst_object_unref (dec);

  return ret;
}

static GstStateChangeReturn
gst_fluilbcdec_change_state (GstElement * element, GstStateChange transition)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (element);
  GstStateChangeReturn ret;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      dec->last_timestamp = 0;
      gst_fluilbcdec_flush (dec, TRUE);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GSTFLU_SETUP_STATISTICS (dec->sinkpad, &dec->stats);
      break;          
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  return ret;
}

static void
gst_fluilbcdec_dispose (GObject * object)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (object);

  if (dec->adapter) {
    gst_adapter_clear (dec->adapter);
    g_object_unref (dec->adapter);
    dec->adapter = NULL;
  }

  if (dec->segment) {
    gst_segment_free (dec->segment);
    dec->segment = NULL;
  }

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_fluilbcdec_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_fluilbcdec_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&fluilbcdec_sink_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&fluilbcdec_src_factory));
  gst_element_class_set_details (element_class, &fluilbcdec_details);
}

static void
gst_fluilbcdec_class_init (GstFluILBCDecClass * klass)
{
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = gst_fluilbcdec_set_property;
  object_class->get_property = gst_fluilbcdec_get_property;

  g_object_class_install_property (object_class, PROP_ENHANCER,
      g_param_spec_boolean ("enhancer", "Enhancer",
          "Enable enhancer", DEFAULT_PROP_ENHANCER, G_PARAM_READWRITE));

  object_class->dispose = gst_fluilbcdec_dispose;
  object_class->finalize = gst_fluilbcdec_finalize;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_fluilbcdec_change_state);
}

static void
gst_fluilbcdec_init (GstFluILBCDec * dec, GstFluILBCDecClass * dec_class)
{
  dec->sinkpad = gst_pad_new_from_static_template (&fluilbcdec_sink_factory,
      "sink");
  gst_pad_set_event_function (dec->sinkpad, gst_fluilbcdec_event);
  gst_pad_set_setcaps_function (dec->sinkpad, gst_fluilbcdec_setcaps);
  gst_pad_set_chain_function (dec->sinkpad, gst_fluilbcdec_chain);
  gst_element_add_pad (GST_ELEMENT (dec), dec->sinkpad);

  dec->srcpad = gst_pad_new_from_static_template (&fluilbcdec_src_factory,
      "src");
  gst_pad_use_fixed_caps (dec->srcpad);
  gst_element_add_pad (GST_ELEMENT (dec), dec->srcpad);

  dec->adapter = gst_adapter_new ();
  dec->segment = gst_segment_new ();
  dec->last_timestamp = 0;
  dec->eos = FALSE;
  dec->packet_count = 0;
  dec->packetsize = 0;
  dec->framesize = 0;
  dec->mode = 0;
  dec->enhancer = DEFAULT_PROP_ENHANCER;
}
