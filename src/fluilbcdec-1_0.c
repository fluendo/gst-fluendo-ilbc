/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

G_DEFINE_TYPE (GstFluILBCDec, gst_fluilbcdec, GST_TYPE_AUDIO_DECODER);

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#define FORMAT_STR "S16LE"
#define FORMAT GST_AUDIO_FORMAT_S16LE
#else
#define FORMAT_STR "S16BE"
#define FORMAT GST_AUDIO_FORMAT_S16BE
#endif

static GstStaticPadTemplate fluilbcdec_src_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw, "
        "format = (string)" FORMAT_STR ", "
        "layout = (string) interleaved, "
        "rate = (int) 8000,"
        "channels = (int) 1")
    );

static GstStaticPadTemplate fluilbcdec_sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-iLBC, mode = (int) { 20, 30 }")
    );

static gboolean
gst_fluilbcdec_set_format (GstAudioDecoder * dec, GstCaps * caps)
{
  GstFluILBCDec *idec = GST_FLUILBCDEC (dec);
  GstAudioInfo info;
  GstStructure *structure;
  gboolean ret = FALSE;
  GstAudioChannelPosition pos[1] = {GST_AUDIO_CHANNEL_POSITION_MONO};

  structure = gst_caps_get_structure (caps, 0);

  if (!gst_structure_get_int (structure, "mode", &idec->mode)) {
    GST_WARNING_OBJECT (idec, "missing mode value in caps");
    goto beach;
  }

  gst_fluilbcdec_setup (idec);
  gst_audio_info_set_format (&info, FORMAT, RATE, CHANNELS, pos);
  gst_audio_decoder_set_output_format (dec, &info);
  ret = TRUE;

beach:
  return ret;
}

static GstFlowReturn
gst_fluilbcdec_parse (GstAudioDecoder * dec, GstAdapter *adapter,
    gint *offset, gint *length)
{
  GstFluILBCDec *idec = GST_FLUILBCDEC (dec);

  /* Push frames in handle_frame with a single packet */
  *offset = 0;
  *length = idec->packetsize;

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_fluilbcdec_handle_frame (GstAudioDecoder * dec, GstBuffer *in)
{
  GstFluILBCDec *idec = GST_FLUILBCDEC (dec);
  GstFlowReturn ret = GST_FLOW_OK;
  GstBuffer *out;
  GstMapInfo map;

  if (in == NULL) {
    return GST_FLOW_OK;
  }

  GST_DEBUG_OBJECT (idec, "Packet number %" G_GUINT64_FORMAT, idec->packet_count);

  /* Decode samples from the input buffer */
  gst_buffer_map (in, &map, GST_MAP_READ);
  iLBC_decode (idec->block, (unsigned char *) map.data, &idec->codec,
      idec->enhancer);
  idec->packet_count++;
  gst_buffer_unmap (in, &map);

  /* Allocate the output buffer */
  out = gst_audio_decoder_allocate_output_buffer(dec, idec->framesize);

  /* Write samples to the output buffer */
  gst_buffer_map (out, &map, GST_MAP_WRITE);
  gst_fluilbcdec_write_samples(idec, (gint16 *) map.data);
  gst_buffer_unmap (out, &map);

  gst_audio_decoder_finish_frame(dec, out, 1);
  return ret;
}

static void
gst_fluilbcdec_class_init (GstFluILBCDecClass * klass)
{
  GstPadTemplate *src_template, *sink_template;
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstAudioDecoderClass *base_class = GST_AUDIO_DECODER_CLASS (klass);

  gobject_class->set_property = gst_fluilbcdec_set_property;
  gobject_class->get_property = gst_fluilbcdec_get_property;

  src_template = gst_static_pad_template_get (&fluilbcdec_src_factory);
  gst_element_class_add_pad_template (element_class, src_template);

  sink_template = gst_static_pad_template_get (&fluilbcdec_sink_factory);
  gst_element_class_add_pad_template (element_class, sink_template);

  gst_element_class_set_static_metadata (element_class,
      PLUGIN_DESC, PLUGIN_CAT, PLUGIN_LONGDESC, PLUGIN_CR);

  g_object_class_install_property (gobject_class, PROP_ENHANCER,
      g_param_spec_boolean ("enhancer", "Enhancer",
          "Enable enhancer", DEFAULT_PROP_ENHANCER, G_PARAM_READWRITE));

  base_class->handle_frame = GST_DEBUG_FUNCPTR (gst_fluilbcdec_handle_frame);
  base_class->set_format = GST_DEBUG_FUNCPTR (gst_fluilbcdec_set_format);
  base_class->parse = GST_DEBUG_FUNCPTR (gst_fluilbcdec_parse);
}

static void
gst_fluilbcdec_init (GstFluILBCDec * dec)
{
  dec->framesize = 0;
  dec->packetsize = 0;
  dec->mode = 0;
  dec->enhancer = DEFAULT_PROP_ENHANCER;
}
