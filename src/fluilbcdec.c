/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#include "fluilbcdec.h"

GST_DEBUG_CATEGORY_EXTERN (gst_fluilbcdec_debug);
#define GST_CAT_DEFAULT gst_fluilbcdec_debug

#define PLUGIN_DESC "Fluendo iLBC Decoder"
#define PLUGIN_CAT "Codec/Decoder/Audio"
#define PLUGIN_LONGDESC "Decode iLBC streams from raw audio samples"
#define PLUGIN_CR "Fluendo S.A. <support@fluendo.com>"

#define RATE 8000
#define CHANNELS 1

enum
{
  PROP_0,
  PROP_ENHANCER
};

#define DEFAULT_PROP_ENHANCER TRUE

static gboolean
gst_fluilbcdec_setup (GstFluILBCDec * dec)
{
  /* Perform decoder setup with the mode */
  initDecode (&dec->codec, dec->mode, dec->enhancer);
  if (dec->mode == 20) {
    dec->packetsize = NO_OF_BYTES_20MS;
  } else if (dec->mode == 30) {
    dec->packetsize = NO_OF_BYTES_30MS;
  }
  dec->framesize = dec->codec.blockl * 2; /* 16 bits per sample */

  return TRUE;
}

static void
gst_fluilbcdec_write_samples (GstFluILBCDec * dec, gint16 * out_data)
{
  gint i;

  /* Convert and write decoded samples */
  for (i = 0; i < dec->codec.blockl; i++) {
    gfloat tmp = dec->block[i];

    if (tmp < MIN_SAMPLE)
      tmp = MIN_SAMPLE;
    else if (tmp > MAX_SAMPLE)
      tmp = MAX_SAMPLE;
    out_data[i] = (gint16) tmp;
  }
}

static void
gst_fluilbcdec_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (object);
  switch (prop_id) {
    case PROP_ENHANCER:{
      dec->enhancer = g_value_get_boolean (value);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}

static void
gst_fluilbcdec_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstFluILBCDec *dec = GST_FLUILBCDEC (object);
  switch (prop_id) {
    case PROP_ENHANCER:
      g_value_set_boolean (value, dec->enhancer);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}

#include <gst/gst.h>
#if GST_CHECK_VERSION(1,0,0)
#include "fluilbcdec-1_0.c"
#else
#include "fluilbcdec-0_10.c"
#endif
