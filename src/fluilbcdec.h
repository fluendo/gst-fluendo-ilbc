/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#ifndef __GST_FLUILBCDEC_H__
#define __GST_FLUILBCDEC_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gst-compat.h"
#include "gst-fluendo.h"

#if GST_CHECK_VERSION(1,0,0)
#include <gst/audio/gstaudiodecoder.h>
#endif

#include "iLBC_define.h"
#include "iLBC_decode.h"

G_BEGIN_DECLS
#define GST_FLUILBCDEC_TYPE            (gst_fluilbcdec_get_type ())
#define GST_FLUILBCDEC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_FLUILBCDEC_TYPE, GstFluILBCDec))
#define GST_FLUILBCDEC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_FLUILBCDEC_TYPE, GstFluILBCDecClass))
#define GST_IS_FLUILBCDEC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_FLUILBCDEC_TYPE))
#define GST_IS_FLUILBCDEC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_FLUILBCDEC_TYPE))
#define GST_FLUILBCDEC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_FLUILBCDEC_TYPE, GstFluILBCDecClass))

typedef struct _GstFluILBCDec GstFluILBCDec;
typedef struct _GstFluILBCDecClass GstFluILBCDecClass;

struct _GstFluILBCDecClass
{
#if GST_CHECK_VERSION(1,0,0)
  GstAudioDecoderClass parent_class;
#else
  GstElementClass parent_class;
#endif
};

struct _GstFluILBCDec
{
#if GST_CHECK_VERSION(1,0,0)
  GstAudioDecoder parent;
#else
  GstElement parent;

  GstPad *sinkpad;
  GstPad *srcpad;

  /* Segment */
  GstSegment *segment;

  gboolean discont;
  gboolean eos;

  GstClockTime last_timestamp;
  GstClockTime duration;

  GstAdapter *adapter;

  GSTFLU_STATISTICS
#endif

  gint mode;
  gboolean enhancer;

  guint32 framesize;
  guint32 packetsize;

  guint64 packet_count;

  iLBC_Dec_Inst_t codec;
  gfloat block[BLOCKL_MAX];
};

GType gst_fluilbcdec_get_type (void);

G_END_DECLS
#endif
