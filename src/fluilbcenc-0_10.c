/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

GST_BOILERPLATE (GstFluILBCEnc, gst_fluilbcenc, GstElement, GST_TYPE_ELEMENT);

static GstElementDetails fluilbcenc_details = {
  PLUGIN_DESC, PLUGIN_CAT, PLUGIN_LONGDESC, PLUGIN_CR
};

static GstStaticPadTemplate fluilbcenc_sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int, "
        "rate = (int) 8000, "
        "channels = (int) 1, "
        "width = (int) 16, "
        "depth = (int) 16, "
        "signed = (boolean) true, "
        "endianness = (int) BYTE_ORDER")
    );

static GstStaticPadTemplate fluilbcenc_src_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-iLBC, mode = (int) { 20, 30 }")
    );

#define GST_FLOW_NEED_MORE_DATA -101

static GType
gst_fluilbcenc_mode_get_type (void)
{
  static GType gst_fluilbcenc_mode_type = 0;
  static GEnumValue gst_fluilbcenc_mode[] = {
    {20, "20 ms", "M20"},
    {30, "30 ms", "M30"},
    {0, NULL, NULL},
  };
  if (!gst_fluilbcenc_mode_type) {
    gst_fluilbcenc_mode_type =
        g_enum_register_static ("GstFluILBCMode", gst_fluilbcenc_mode);
  }
  return gst_fluilbcenc_mode_type;
}

#define GST_FLUILBCENC_MODE_TYPE (gst_fluilbcenc_mode_get_type())
#define DEFAULT_PROP_MODE 20

enum
{
  PROP_0,
  PROP_MODE
};

static gboolean
gst_fluilbcenc_setcaps (GstPad * pad, GstCaps * caps)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (gst_pad_get_parent (pad));
  gboolean ret = FALSE;
  GstCaps *out_caps;

  GST_DEBUG_OBJECT (enc, "setcaps called with %" GST_PTR_FORMAT, caps);

  gst_fluilbcenc_setup (enc);

  out_caps = gst_caps_new_simple ("audio/x-iLBC",
      "mode", G_TYPE_INT, enc->mode, NULL);

  gst_pad_set_caps (enc->srcpad, out_caps);
  gst_caps_unref (out_caps);

  enc->duration = gst_util_uint64_scale (enc->framesize >> 1, GST_SECOND, RATE);

  ret = TRUE;

  gst_object_unref (enc);
  return ret;
}

static GstFlowReturn
gst_fluilbcenc_encode (GstFluILBCEnc * enc)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstBuffer *buffer;
  const gint16 *in_data = NULL;
  guint32 out_size = enc->packetsize;
  guint32 framesize = enc->framesize;
  GstClockTime timestamp = GST_CLOCK_TIME_NONE;

  /* check if there's enough audio data in the adapter */
  if (gst_adapter_available (enc->adapter) < framesize) {
    GST_LOG_OBJECT (enc, "not enough data for a frame, required %d bytes",
        framesize);
    ret = GST_FLOW_NEED_MORE_DATA;
    goto beach;
  }
  
  /* calculate timestamp and duration */
  timestamp = enc->last_timestamp;
  if (enc->last_timestamp != -1) {
    enc->last_timestamp += enc->duration;
  }

  /* peek the audio samples */
  in_data = (gint16 *) gst_adapter_peek (enc->adapter, framesize);
  /* Convert into float and write it in the output buffer*/
  gst_fluilbcenc_read_samples (enc, in_data);

  /* flush the adapter for encoded data */
  gst_adapter_flush (enc->adapter, framesize);
  /* allocate a GstBuffer for ILBC coded data */
  buffer = gst_buffer_new_and_alloc (out_size);
  if (!GST_IS_BUFFER (buffer)) {
    GST_WARNING_OBJECT (enc, "failed allocating a bytes buffer");
    ret = GST_FLOW_ERROR;
    goto beach;
  }
  
  /* encode into the buffer */
  iLBC_encode (GST_BUFFER_DATA (buffer), enc->block, &enc->codec);
  /* if errors go for next */
  GST_BUFFER_SIZE (buffer) = enc->codec.no_of_bytes;
  GST_BUFFER_TIMESTAMP (buffer) = timestamp;
  GST_BUFFER_DURATION (buffer) = enc->duration;
  
  if (enc->discont) {
    GST_BUFFER_FLAG_SET (buffer, GST_BUFFER_FLAG_DISCONT);
    enc->discont = FALSE;
  }
  gst_buffer_set_caps (buffer, GST_PAD_CAPS (enc->srcpad));

  /* push the buffer in the srcpad */
  ret = GSTFLU_PAD_PUSH (enc->srcpad, buffer, &enc->stats);

beach:
  return ret;
}

static GstFlowReturn
gst_fluilbcenc_chain (GstPad * pad, GstBuffer * buffer)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (gst_pad_get_parent (pad));
  GstFlowReturn ret = GST_FLOW_OK;

  /* discontinuity clears adapter, FIXME, maybe we can set some
   * encoder flag to mask the discont. */
  if (GST_BUFFER_FLAG_IS_SET (buffer, GST_BUFFER_FLAG_DISCONT)) {
    gst_adapter_clear (enc->adapter);
    enc->last_timestamp = 0;
    enc->discont = TRUE;
  }

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer)) {
    enc->last_timestamp = GST_BUFFER_TIMESTAMP (buffer);
  }

  gst_adapter_push (enc->adapter, buffer);

  while (ret == GST_FLOW_OK) {
    ret = gst_fluilbcenc_encode (enc);
  }
  if (ret == GST_FLOW_NEED_MORE_DATA) {
    GST_LOG_OBJECT (enc, "need more data");
    ret = GST_FLOW_OK;
  }

  gst_object_unref (enc);

  return ret;
}

static void
gst_fluilbcenc_flush (GstFluILBCEnc * enc, gboolean discard)
{
  if (!discard) {
    guint avail = gst_adapter_available (enc->adapter);
    if (avail && enc->framesize) {
      guint size = enc->framesize - avail;
      GstBuffer *buffer = gst_buffer_new_and_alloc (size);
      memset (GST_BUFFER_DATA (buffer), 0, size);
      gst_adapter_push (enc->adapter, buffer);
      gst_fluilbcenc_encode (enc);
    }
  }
  enc->last_timestamp = 0;
  enc->discont = FALSE;
  enc->eos = FALSE;
  gst_adapter_clear (enc->adapter);
}

static gboolean
gst_fluilbcenc_event (GstPad * pad, GstEvent * event)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (gst_pad_get_parent (pad));
  gboolean ret = FALSE;

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
    {
      GST_DEBUG_OBJECT (enc, "we are EOS");
      /* Flush with discard because it is the behaviour on the reference encoder */
      gst_fluilbcenc_flush (enc, TRUE);
      enc->eos = TRUE;
      ret = gst_pad_event_default (pad, event);
      break;
    }
    case GST_EVENT_NEWSEGMENT:
    {
      GstFormat format;
      gdouble rate;
      gint64 start, stop, time;
      gboolean update;

      gst_event_parse_new_segment (event, &update, &rate, &format, &start,
          &stop, &time);

      GST_DEBUG_OBJECT (enc, "received new segment from %" GST_TIME_FORMAT
          " to %" GST_TIME_FORMAT, GST_TIME_ARGS (start), GST_TIME_ARGS (stop));

      if (!update)
        gst_fluilbcenc_flush (enc, FALSE);

      if (format == GST_FORMAT_BYTES) {
        format = GST_FORMAT_TIME;
        if (start == -1) {
          start = 0;
        } else {
          start = gst_util_uint64_scale (start >> 1, GST_SECOND, RATE);
        }
        if (stop != -1) {
          stop = gst_util_uint64_scale (stop >> 1, GST_SECOND, RATE);
        }
        if (time != -1) {
          time = gst_util_uint64_scale (time >> 1, GST_SECOND, RATE);
        }
        gst_event_unref (event);
        event = gst_event_new_new_segment (update, rate, format,
            start, stop, time);
      } else if (format != GST_FORMAT_TIME) {
        format = GST_FORMAT_TIME;
        start = time = 0;
        stop = -1;
        gst_event_unref (event);
        event = gst_event_new_new_segment (update, rate, format,
            start, stop, time);
      }

      /* now copy over the values */
      gst_segment_set_newsegment (enc->segment, update, rate, format, start,
          stop, time);

      if (!update && format == GST_FORMAT_TIME)
        enc->last_timestamp = start;

      ret = gst_pad_event_default (pad, event);
      break;
    }
    case GST_EVENT_FLUSH_STOP:
      GST_DEBUG_OBJECT (enc, "flushing");

      gst_fluilbcenc_flush (enc, TRUE);

      /* Need to init our segment again after a flush */
      gst_segment_init (enc->segment, GST_FORMAT_TIME);

      ret = gst_pad_event_default (pad, event);
      break;
    default:
      ret = gst_pad_event_default (pad, event);
      break;
  }

  gst_object_unref (enc);

  return ret;
}

static GstStateChangeReturn
gst_fluilbcenc_change_state (GstElement * element, GstStateChange transition)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (element);

  GstStateChangeReturn ret;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      enc->last_timestamp = 0;
      gst_fluilbcenc_flush (enc, TRUE);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GSTFLU_SETUP_STATISTICS (enc->sinkpad, &enc->stats);
      break;      
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  return ret;
}

static void
gst_fluilbcenc_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (object);

  switch (prop_id) {
    case PROP_MODE:
      enc->mode = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}

static void
gst_fluilbcenc_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (object);

  switch (prop_id) {
    case PROP_MODE:
      g_value_set_enum (value, enc->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}

static void
gst_fluilbcenc_dispose (GObject * object)
{
  GstFluILBCEnc *enc = GST_FLUILBCENC (object);

  if (enc->adapter) {
    gst_adapter_clear (enc->adapter);
    g_object_unref (enc->adapter);
    enc->adapter = NULL;
  }

  if (enc->segment) {
    gst_segment_free (enc->segment);
    enc->segment = NULL;
  }

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_fluilbcenc_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_fluilbcenc_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&fluilbcenc_sink_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&fluilbcenc_src_factory));
  gst_element_class_set_details (element_class, &fluilbcenc_details);
}

static void
gst_fluilbcenc_class_init (GstFluILBCEncClass * klass)
{
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = gst_fluilbcenc_set_property;
  object_class->get_property = gst_fluilbcenc_get_property;

  g_object_class_install_property (object_class, PROP_MODE,
      g_param_spec_enum ("mode", "mode",
          "Frame Size Mode (in miliseconds)", GST_FLUILBCENC_MODE_TYPE,
          DEFAULT_PROP_MODE, G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  object_class->dispose = gst_fluilbcenc_dispose;
  object_class->finalize = gst_fluilbcenc_finalize;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_fluilbcenc_change_state);
}

static void
gst_fluilbcenc_init (GstFluILBCEnc * enc, GstFluILBCEncClass * enc_class)
{
  enc->sinkpad = gst_pad_new_from_static_template (&fluilbcenc_sink_factory,
      "sink");
  gst_pad_set_event_function (enc->sinkpad, gst_fluilbcenc_event);
  gst_pad_set_setcaps_function (enc->sinkpad, gst_fluilbcenc_setcaps);
  gst_pad_set_chain_function (enc->sinkpad, gst_fluilbcenc_chain);
  gst_element_add_pad (GST_ELEMENT (enc), enc->sinkpad);

  enc->srcpad = gst_pad_new_from_static_template (&fluilbcenc_src_factory,
      "src");
  gst_pad_use_fixed_caps (enc->srcpad);
  gst_element_add_pad (GST_ELEMENT (enc), enc->srcpad);

  enc->adapter = gst_adapter_new ();
  enc->segment = gst_segment_new ();
  enc->last_timestamp = 0;
  enc->eos = FALSE;
  enc->mode = DEFAULT_PROP_MODE;
}
