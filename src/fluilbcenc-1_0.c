/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

G_DEFINE_TYPE (GstFluILBCEnc, gst_fluilbcenc, GST_TYPE_AUDIO_ENCODER);

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#define FORMAT_STR "S16LE"
#define FORMAT GST_AUDIO_FORMAT_S16LE
#else
#define FORMAT_STR "S16BE"
#define FORMAT GST_AUDIO_FORMAT_S16BE
#endif

static GstStaticPadTemplate fluilbcenc_sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw, "
        "format = (string)" FORMAT_STR ", "
        "layout = (string) interleaved, "
        "rate = (int) 8000,"
        "channels = (int) 1")
    );

static GstStaticPadTemplate fluilbcenc_src_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-iLBC, mode = (int) { 20, 30 }")
    );


/* check downstream caps to configure format */
static gboolean
gst_fluilbcenc_negotiate (GstAudioEncoder * enc)
{
  GstFluILBCEnc *ienc = GST_FLUILBCENC(enc);
  GstCaps *caps = NULL, *out_caps = NULL;
  GstStructure *s;

  caps = gst_pad_get_allowed_caps (GST_AUDIO_ENCODER_SRC_PAD (enc));
  s = gst_caps_get_structure (caps, 0);

  if (!caps || gst_caps_get_size(caps) <= 0)
    return TRUE;

  GST_DEBUG_OBJECT (ienc, "Negotiting with caps:%" GST_PTR_FORMAT, caps);

  /* Check the profiles */
  if (!gst_structure_get_uint (s, "mode", &ienc->mode)) {
    ienc->mode = 20;
  }

  out_caps = gst_caps_new_simple ("audio/x-iLBC",
      "mode", G_TYPE_INT, ienc->mode, NULL);

  gst_audio_encoder_set_output_format (enc, out_caps);
  gst_caps_unref (out_caps);

  return TRUE;
}

static gboolean
gst_fluilbcenc_set_format (GstAudioEncoder * enc, GstAudioInfo * info)
{
  GstFluILBCEnc *ienc = GST_FLUILBCENC (enc);

  if (!gst_fluilbcenc_negotiate (enc))
    return FALSE;

  if (!gst_fluilbcenc_setup (ienc))
    return FALSE;

  /* Request frames of exactly 20 or 30 ms dependending on the encoder mode */ 
  gst_audio_encoder_set_frame_samples_max (enc, ienc->framesize / 2);
  gst_audio_encoder_set_frame_samples_min (enc, ienc->framesize / 2);
  gst_audio_encoder_set_frame_max (enc, 1);

  return TRUE;
}

static gboolean
gst_fluilbcenc_start (GstAudioEncoder *enc)
{
  return TRUE;
}

static gboolean
gst_fluilbcenc_stop (GstAudioEncoder *enc)
{
  return TRUE;
}

static GstFlowReturn
gst_fluilbcenc_handle_frame (GstAudioEncoder * enc, GstBuffer *in)
{
  GstFluILBCEnc *ienc = GST_FLUILBCENC (enc);
  GstBuffer *out;
  GstMapInfo map;

  if (in == NULL) {
    return GST_FLOW_OK;
  }

  /* Read samples from the input buffer */
  gst_buffer_map (in, &map, GST_MAP_READ);
  gst_fluilbcenc_read_samples (ienc, (gint16 *) map.data);
  gst_buffer_unmap (in, &map);

  /* Allocate output buffer */
  out = gst_audio_encoder_allocate_output_buffer (enc, ienc->packetsize);

  /* Encode samples into the output buffer */
  gst_buffer_map (out, &map, GST_MAP_WRITE);
  iLBC_encode (map.data, ienc->block, &ienc->codec);
  gst_buffer_unmap (out, &map);

  return gst_audio_encoder_finish_frame(enc, out, ienc->framesize / 2);
}

static void
gst_fluilbcenc_class_init (GstFluILBCEncClass * klass)
{
  GstPadTemplate *src_template, *sink_template;
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstAudioEncoderClass *base_class = GST_AUDIO_ENCODER_CLASS (klass);

  src_template = gst_static_pad_template_get (&fluilbcenc_src_factory);
  gst_element_class_add_pad_template (element_class, src_template);

  sink_template = gst_static_pad_template_get (&fluilbcenc_sink_factory);
  gst_element_class_add_pad_template (element_class, sink_template);

  gst_element_class_set_static_metadata (element_class,
      PLUGIN_DESC, PLUGIN_CAT, PLUGIN_LONGDESC, PLUGIN_CR);

  base_class->handle_frame = GST_DEBUG_FUNCPTR (gst_fluilbcenc_handle_frame);
  base_class->set_format = GST_DEBUG_FUNCPTR (gst_fluilbcenc_set_format);
  base_class->start = GST_DEBUG_FUNCPTR (gst_fluilbcenc_start);
  base_class->stop = GST_DEBUG_FUNCPTR (gst_fluilbcenc_stop);
}

static void
gst_fluilbcenc_init (GstFluILBCEnc * enc)
{
  enc->framesize = 0;
  enc->packetsize = 0;
  enc->mode = 0;
}
