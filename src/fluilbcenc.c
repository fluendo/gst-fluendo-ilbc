/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#include "fluilbcenc.h"

#define PLUGIN_DESC "Fluendo iLBC Encoder"
#define PLUGIN_CAT "Codec/Encoder/Audio"
#define PLUGIN_LONGDESC "Encode iLBC streams from raw audio samples"
#define PLUGIN_CR "Fluendo S.A. <support@fluendo.com>"

GST_DEBUG_CATEGORY_EXTERN (gst_fluilbcenc_debug);
#define GST_CAT_DEFAULT gst_fluilbcenc_debug

#define RATE 8000
#define CHANNELS 1

static gboolean
gst_fluilbcenc_setup (GstFluILBCEnc * enc)
{
  initEncode (&enc->codec, enc->mode);
  if (enc->mode == 20) {
    enc->packetsize = NO_OF_BYTES_20MS;
  } else if (enc->mode == 30) {
    enc->packetsize = NO_OF_BYTES_30MS;
  }
  enc->framesize = enc->codec.blockl * 2;  /* 16 bits per sample */


  return TRUE;
}

static void
gst_fluilbcenc_read_samples (GstFluILBCEnc *enc, const gint16 *in_data) {
  int i;

  /* Convert into float */
  for (i = 0; i < enc->codec.blockl; i++) {
    enc->block[i] = (gfloat) in_data[i];
  }
}

#if GST_CHECK_VERSION(1,0,0)
#include "fluilbcenc-1_0.c"
#else
#include "fluilbcenc-0_10.c"
#endif
