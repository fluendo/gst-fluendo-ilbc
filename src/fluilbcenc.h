/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#ifndef __GST_FLUILBCENC_H__
#define __GST_FLUILBCENC_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gst-compat.h"
#include "gst-fluendo.h"

#if GST_CHECK_VERSION(1,0,0)
#include <gst/audio/gstaudioencoder.h>
#endif

#include "iLBC_define.h"
#include "iLBC_encode.h"

G_BEGIN_DECLS

#define GST_FLUILBCENC_TYPE            (gst_fluilbcenc_get_type ())
#define GST_FLUILBCENC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_FLUILBCENC_TYPE, GstFluILBCEnc))
#define GST_FLUILBCENC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_FLUILBCENC_TYPE, GstFluILBCEncClass))
#define GST_IS_FLUILBCENC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_FLUILBCENC_TYPE))
#define GST_IS_FLUILBCENC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_FLUILBCENC_TYPE))
#define GST_FLUILBCENC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_FLUILBCENC_TYPE, GstFluILBCEncClass))

typedef struct _GstFluILBCEnc GstFluILBCEnc;
typedef struct _GstFluILBCEncClass GstFluILBCEncClass;

struct _GstFluILBCEncClass
{
#if GST_CHECK_VERSION(1,0,0)
  GstAudioEncoderClass parent_class;
#else
  GstElementClass parent_class;
#endif
};

struct _GstFluILBCEnc
{
#if GST_CHECK_VERSION(1,0,0)
  GstAudioEncoder parent;
#else
  GstElement parent;

  GstPad *sinkpad;
  GstPad *srcpad;

  /* Segment */
  GstSegment *segment;

  gboolean discont;
  gboolean eos;

  GstClockTime last_timestamp;
  GstClockTime duration;

  GstAdapter *adapter;
#endif

  guint mode;
  guint32 packetsize;
  guint32 framesize;
  gint64 packet_count;

  iLBC_Enc_Inst_t codec;
  gfloat block[BLOCKL_MAX];

};

GType gst_fluilbcenc_get_type (void);


G_END_DECLS
#endif
