/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#include <gst/gst.h>
#include <string.h>
#include <stdlib.h>

#include "config.h"
#include "fluilbcenc.h"

GstBuffer *out_buffer;

GstFlowReturn
out_chain (GstPad *pad, GstBuffer *buffer) {
  out_buffer = buffer;
  return GST_FLOW_OK;
}

void test_ilbcenc ()
{
  GstElement *element;
  GstPad *srcpad, *sinkpad, *peerpad;
  GstCaps *srccaps, *sinkcaps;
  GstBuffer *in_buffer;
  guint8 in_random[320];
  
  in_buffer = gst_buffer_new ();
  GST_BUFFER_SIZE (in_buffer) = 320;
  
  int i;
  srand(time(NULL));
  for (i = 0; i < 320; i++) {
    in_random[i] = (rand() % 256);
  }
  GST_BUFFER_DATA (in_buffer) = in_random;

  srccaps = gst_caps_from_string ("audio/x-raw-int, rate = (int) 8000, channels = (int) 1");
  sinkcaps = gst_caps_from_string ("audio/x-iLBC, mode = (int) 20");
  GST_BUFFER_CAPS (in_buffer) = gst_caps_ref (srccaps);
  
  element = gst_element_factory_make ("fluilbcenctest", NULL);
  
  srcpad = gst_pad_new (NULL, GST_PAD_SRC);
  gst_pad_set_caps (srcpad, srccaps);
  peerpad = gst_element_get_static_pad (element, "sink");
  
  gst_pad_link (srcpad, peerpad);
  gst_pad_set_active (srcpad, TRUE);

  sinkpad = gst_pad_new (NULL, GST_PAD_SINK);
  gst_pad_set_caps (sinkpad, sinkcaps);
  gst_pad_set_chain_function (sinkpad, out_chain);
  peerpad = gst_element_get_static_pad (element, "src");
  
  gst_pad_link (peerpad, sinkpad);
  gst_pad_set_active (sinkpad, TRUE);

  out_buffer = NULL;
  gst_element_set_state (GST_ELEMENT (element), GST_STATE_PLAYING);
  g_assert (gst_pad_push (srcpad, in_buffer) == GST_FLOW_OK);
  gst_element_set_state (GST_ELEMENT (element), GST_STATE_NULL);

  /* Check our element */
  g_assert (out_buffer != NULL);
  g_assert (GST_BUFFER_SIZE (out_buffer) == 38);
}

static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "fluilbcenctest", GST_RANK_PRIMARY+1,
      gst_fluilbcenc_get_type ()))
    return FALSE;

  return TRUE;
}

int main(int argc, char *argv[])
{
  gst_init(&argc, &argv);
    
  g_test_init (&argc, &argv, NULL);
  
  gst_plugin_register_static (0, 10, "test", "test descr", plugin_init, "10", "Proprietary", "asd", "asd", "asd");
  
  g_test_add_func ("/Speech/Ilbc/Enc", test_ilbcenc);

  g_test_run ();

  return 0;
}

