#!/bin/bash

DISTFILES=distfiles
TMPDIR=tmp
SAMPLESDIR=samples
ILBC="iLBCfreewareTestFiles.zip"
ILBCURI="http://www.ilbcfreeware.org/documentation/$ILBC"
# tmp files
gst=$TMPDIR/bit.gst
modes=(20 30)

### SOME COLORS !
RED='\e[31m'
GRE='\e[32m'
YEL='\e[33m'
BLU='\e[34m'
MAG='\e[35m'
CYA='\e[36m'
B='\e[1m'
NOR='\e[0m'

function dependency_failed()
{
	echo "not found, please install it"
	exit
}


function check_dependency()
{
	echo -n "Checking for $1 ... "
	dep=`which $1`
	if [ $? -ne 0 ]; then
		dependency_failed $1
	fi
	echo "ok"
}

function check_dependencies()
{
	check_dependency wget
	check_dependency unzip
	check_dependency gst-launch-0.10
}

function uncompress()
{
	if [ ! -e $DISTFILES ]; then
		mkdir $DISTFILES
	fi
	wget -P $DISTFILES -c $ILBCURI
  # uncompress the needed zips for the input and output files
  if [ ! -e $SAMPLESDIR ]; then
    mkdir $SAMPLESDIR
  fi
	unzip -uo $DISTFILES/$ILBC -d $SAMPLESDIR 
  # make a tmp dir
	if [ ! -e $TMPDIR ]; then
		mkdir $TMPDIR
	fi
  rm $SAMPLESDIR/iLBC_test.exe
}

function build_utils()
{
  (cd utils && make)
}

function makeSamples()
{
  #remove old samples
  cd samples
  # remove encoder output
  rm *.BIT
  # remove decoder output
  rm *.OUT
  # remove channel dirt files
  rm *.chn
  # for general cleanness
  rm iLBC_test.txt

  sh ../utils/iLBC_test.sh 20 iLBC.INP iLBC_20ms.BIT iLBC_20ms_clean.OUT
  sh ../utils/iLBC_test.sh 30 iLBC.INP iLBC_30ms.BIT iLBC_30ms_clean.OUT

  # tests to see if the fluendo libs provide the same result
  #sh ../fluendo_iLBC/iLBC_test.sh 20 iLBC.INP iLBC_20ms.BIT iLBC_20ms_clean.OUT
  #sh ../fluendo_iLBC/iLBC_test.sh 30 iLBC.INP iLBC_30ms.BIT iLBC_30ms_clean.OUT
  
  cd ..
}

function hashandsize()
{
	#md5sum check
	md5compare $1 $2
	if [ $md5result -eq 1 ]; then
	 	#passed md5sum
    echo -e "md5sum$GRE ok$NOR"
    continue;
  fi
	# Check that the files size are the same
  filesizecheck $1 $2
	if [ $size_chk_result -ne 1 ]; then
		echo "File size does not match ($size_amr != $size_gst)"
		continue
	fi
}

function checkresultsdecode()
{
  hashandsize $1 $2
	if [ $md5result -ne 1 ]; then
   	#do a byte by byte compare with tolerance as we are decoding
  	cmpcomparedecode $1 $2
  fi	  
}

function filesizecheck()
{
  size_gst=`wc -c $1 | cut -d " " -f 1`
	size_amr=`wc -c $2 | cut -d " " -f 1`
	if [ $size_gst -ne $size_amr ]; then
	  size_chk_result=0
	else
	  size_chk_result=1
	fi
}

function md5compare()
{
  file1sum=`md5sum $1 | cut -d " " -f 1`
  file2sum=`md5sum $2 | cut -d " " -f 1`
  if [ $file1sum == $file2sum ]; then
    md5result=1
  else
    md5result=0
  fi
}

function cmpcomparedecode()
{
	# when checking for errors, check the different bytes and the values
	# maximum allowed difference is of 1 bit
	# we can't count the errors on a | while read ... stupid bug
	#cmp -l $orig_out $TMPDIR/out.gst | tr -s " " | while read line orig ours
	#do
	errors=0
	IFS=$'\n'
	for line in `cmp -l $2 $gst | tr -s " " | cut -d " " -f 3-`; do
		# doing a cut here costs a lot, doing a read doesnt work
		#origbyte=`echo $line | cut -d " " -f 2`
		#oursbyte=`echo $line | cut -d " " -f 3`
		# again some bashims
		origbyte=${line% *}
		oursbyte=${line#* }
		let errordiff=$origbyte-$oursbyte
		if [ $errordiff -gt 1 ] || [ $errordiff -lt -1 ]; then
			errors=$((errors + 1))
		fi
	done
	unset IFS
  # Number chosen uniquely to pass all tests until we determine a better way of counting 
	if [ $errors -gt 108 ]; then
		echo -e "$RED$errors$NOR ($size_amr)"
	else
		echo -e "$GRE$errors$NOR ($size_amr)"
	fi
}

function check_ilbc_encode()
{
  echo -e "${YEL}iLBC encode test${NOR}"
	files=`find $SAMPLESDIR/ -name *.INP`
	for i in $files; do
		for n in `seq 0 1`; do
			orig="$SAMPLESDIR/iLBC_${modes[n]}ms.BIT"
			if [ ! -e $orig ]; then
				echo "File is missing $orig"
				continue;
			fi
      gst-launch-0.10 filesrc location=$i ! audioparse rate=8000 width=16 depth=16 channels=1 ! fluilbcenc mode=${modes[n]} ! filesink location=$gst > /dev/null 2>&1
      echo -ne " File $BLU$i$NOR -"
			echo -n " mode ${modes[n]}ms: "
      checkresultsdecode $gst $orig
		done
	done
	#summary
	:
}

function check_ilbc_decode()
{
	echo -e "${YEL}iLBC decode test${NOR}"
	files=`find $SAMPLESDIR/ -name \*.BIT`
	for i in $files; do
		for n in `seq 0 1`; do
			orig_in="$SAMPLESDIR/iLBC_${modes[n]}ms.BIT"
			orig_out="$SAMPLESDIR/iLBC_${modes[n]}ms_clean.OUT"
			if [ ! -e $orig_in ]; then
        echo "File is missing $orig_in"
				continue;
			fi
			if [ ! -e $orig_out ]; then
        echo "File is missing $orig_out"
				continue;
			fi
			gst-launch-0.10 filesrc location=$orig_in ! audio/x-iLBC,rate=8000,channels=1,mode=${modes[n]} ! fluilbcdec ! filesink location=$gst > /dev/null 2>&1
      echo -ne " File $BLU$i$NOR -"
			echo -n " band ${modes[n]}: "
			checkresultsdecode $gst $orig_out
		done
	done
	#summary
}

function clean()
{
	rm -rf $TMPDIR
	rm -rf $SAMPLESDIR
	rm -rf $DISTFILES
    cd utils/
    make clean
}

function help()
{
	echo "iLBC Conformance test"
	echo "====================="
	echo "./run.sh COMMAND"
	echo "Where COMMAND can be one of the following:"
	echo
	echo "help         This"
	echo "all          All tests"
        echo "dec          Only decoder tests"
        echo "enc          Only encoder tests"
	echo "clean        Clean up the files"
}

function check_args()
{
	case "$1" in
    all)
      check_dependencies
      uncompress
      build_utils
      makeSamples
      check_ilbc_encode
      check_ilbc_decode
  	;;
    dec)
      check_dependencies
      uncompress
      build_utils
      makeSamples
      check_ilbc_decode
    ;;
    enc)
      check_dependencies
      uncompress
      build_utils
      makeSamples
      check_ilbc_encode
    ;;
    clean)
      clean
    ;;
    *)
      help
      exit 1
    ;;
    esac
}

if [ $# -lt 1 ]; then
	help
	exit 0
fi

check_args $1
